uclog
=====

# Overview

Uclog is a small package to distribute log searching among multiple
hosts and multiple log sources. Hosts in a cluster discover each other
using a gossip protocol. Queries are distributed to all known hosts,
and the results aggregated.

# Installation

Once you have downloaded the source code, run the following commands:

    $ go get -v ./...      # Install dependencies
    $ go install -v ./...  # Installs binaries to $GOPATH/bin

# Usage

Describe the various local log sources on your system by creating a
configuration file, for example `/etc/uclogd.conf`:

    # Just an example config.
    # Standard syslog log files.
    syslog:/var/log/syslog
    syslog:/var/log/auth.log

    # Example for a runit-managed daemon.
    runit:/var/log/my-service

Start the server (`uclogd`) on your nodes, using the `--peers` option
to make them aware of each other, passing at least one known host to
each new node.

It will be then possible to query the logs using `ucgrep` on any of
the cluster nodes, or from a remote machine using the `--server`
option.
