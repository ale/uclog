package uclog

import (
	"bufio"
	"bytes"
	"compress/gzip"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"sort"
	"strings"
	"sync"
	"time"
)

var localhost string

// An io.ReadCloser that supports gzip compression.
type gzipFile struct {
	*gzip.Reader
	file *os.File
}

func newGzipFile(filename string) (*gzipFile, error) {
	file, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	gzipr, err := gzip.NewReader(file)
	if err != nil {
		file.Close()
		return nil, err
	}
	return &gzipFile{gzipr, file}, nil
}

func (g *gzipFile) Close() error {
	g.Reader.Close()
	return g.file.Close()
}

type logFileInfo struct {
	Path string
	End  time.Time
}

type logFileInfoList []logFileInfo

func (l logFileInfoList) Len() int      { return len(l) }
func (l logFileInfoList) Swap(i, j int) { l[i], l[j] = l[j], l[i] }
func (l logFileInfoList) Less(i, j int) bool {
	return l[i].End.Before(l[j].End)
}

// Container of arbitrary key=value source options.
type SourceOptions struct {
	values map[string]string
}

// Get the value of an option (or possibly the default). Can be called
// with a nil receiver.
func (o *SourceOptions) Get(key, defaultValue string) string {
	if o == nil {
		return defaultValue
	}
	if value, ok := o.values[key]; ok {
		return value
	}
	return defaultValue
}

type multicloser struct {
	io.Reader
	toclose []io.ReadCloser
}

func (m *multicloser) Close() error {
	for _, r := range m.toclose {
		r.Close()
	}
	return nil
}

func openLog(filename string) (io.ReadCloser, error) {
	if strings.HasSuffix(filename, ".gz") {
		return newGzipFile(filename)
	}
	return os.Open(filename)
}

// Create a single sequential io.ReadCloser out of multiple log files.
func openLogFiles(files []logFileInfo) (io.ReadCloser, error) {
	var readers []io.Reader
	var closers []io.ReadCloser
	for _, l := range files {
		if f, err := openLog(l.Path); err == nil {
			readers = append(readers, f)
			closers = append(closers, f)
		}
	}
	if len(readers) == 0 {
		return nil, errors.New("no files could be found")
	}
	m := io.MultiReader(readers...)
	return &multicloser{Reader: m, toclose: closers}, nil
}

// A fileScanner can represent a file-based source spread across
// multiple files (as it is the case with log rotation, for instance).
type fileScanner interface {
	ScanFiles() ([]logFileInfo, error)
}

// A lineParser scans a line-based log source.
type lineParser interface {
	Parse([]byte) (*Log, error)
}

// fileBasedSource ties together a file scanner and a line-oriented
// parser, so it is suitable for many kinds of file-based logging.
// Scanners and parsers are separate interfaces.
type fileBasedSource struct {
	guard     *guardFile
	allFiles  []logFileInfo
	filesLock sync.Mutex
	scanner   fileScanner
	parser    lineParser
}

type fileQueryIterator struct {
	rc     io.ReadCloser
	rows   *bufio.Scanner
	parser lineParser
	query  Query
	cur    *Log
}

// Next advances to the next matching record. Returns false on EOF.
func (i *fileQueryIterator) Next() bool {
	for i.rows.Scan() {
		log, err := i.parser.Parse(i.rows.Bytes())
		if err != nil {
			continue
		}
		if i.query.Match(log) {
			i.cur = log
			return true
		}
	}
	// We're at EOF, cleanup.
	i.rc.Close()
	return false
}

// Log returns the current log.
func (i *fileQueryIterator) Log() *Log {
	return i.cur
}

// RunQuery evaluates the given Query on this source, and returns an
// iterator over the results.
func (s *fileBasedSource) RunQuery(query Query) (QueryResults, error) {
	files, err := s.files(query.TimeRange())
	if err != nil {
		return nil, err
	}
	in, err := openLogFiles(files)
	if err != nil {
		return nil, err
	}

	return &fileQueryIterator{
		rc:     in,
		rows:   bufio.NewScanner(in),
		parser: s.parser,
		query:  query,
	}, nil
}

// Return the list of log files covering a specific time range.
func (s *fileBasedSource) files(t TimeRange) ([]logFileInfo, error) {
	logfiles, err := s.getAllFiles()
	if err != nil {
		return nil, err
	}

	var cur TimeRange
	var out []logFileInfo
	for _, l := range logfiles {
		cur.End = l.End
		if t.Overlaps(cur) {
			out = append(out, l)
		}
		cur.Start = l.End
	}
	return out, nil
}

// Return the (possibly cached) list of known log files for this source.
func (s *fileBasedSource) getAllFiles() ([]logFileInfo, error) {
	s.filesLock.Lock()
	defer s.filesLock.Unlock()

	// Cache the list of files until the guard file is rotated.
	if s.guard.Changed() {
		files, err := s.scanner.ScanFiles()
		if err != nil {
			return nil, err
		}

		// Make sure that the file list is sorted by
		// ascending end time (so that concatenating them
		// respects the time ordering).
		sort.Sort(logFileInfoList(files))

		// Tamper with the end of the time range of the last
		// (most  recent) log so that it extends to infinity.
		files[len(files)-1].End = time.Time{}

		s.allFiles = files
	}

	return s.allFiles, nil
}

func newFileBasedSource(guard string, scanner fileScanner, parser lineParser) Source {
	return &fileBasedSource{
		guard:   newGuardFile(guard),
		scanner: scanner,
		parser:  parser,
	}
}

// A scanner for logs created by 'runit'.
type runitScanner struct {
	Dir string
}

func (s *runitScanner) ScanFiles() ([]logFileInfo, error) {
	dir, err := os.Open(s.Dir)
	if err != nil {
		return nil, err
	}
	defer dir.Close()
	files, err := dir.Readdir(-1)
	if err != nil {
		return nil, err
	}

	var lfi []logFileInfo
	for _, f := range files {
		if f.Name() != "current" && !strings.HasPrefix(f.Name(), "@") {
			continue
		}
		lfi = append(lfi, logFileInfo{
			Path: filepath.Join(s.Dir, f.Name()),
			End:  f.ModTime(),
		})
	}
	return lfi, nil
}

const runitTimestampFormat = "2006-01-02T15:04:05"

// Parser for 'runit' logs.
type runitLineParser struct {
	host    string
	program string
}

func (p *runitLineParser) Parse(line []byte) (*Log, error) {
	if len(line) < 27 {
		return nil, errors.New("line too short")
	}
	// The Go time package interprets '_' so we first have to
	// replace it with a different character!
	line[10] = byte('T')

	ts, err := time.ParseInLocation(runitTimestampFormat, string(line[:25]), time.Local)
	if err != nil {
		return nil, err
	}

	return &Log{
		Stamp:   ts,
		Host:    p.host,
		Program: p.program,
		Message: string(line[26:len(line)]),
	}, nil
}

func newRunitSource(dir string, opts *SourceOptions) Source {
	return newFileBasedSource(
		filepath.Join(dir, "current"),
		&runitScanner{Dir: dir},
		&runitLineParser{
			host:    opts.Get("host", localhost),
			program: opts.Get("prog", ""),
		})
}

// A scanner for generic log files, supporting log rotation (with
// optional compression).
type logfileScanner struct {
	File string
}

func (s *logfileScanner) ScanFiles() ([]logFileInfo, error) {
	f, err := os.Stat(s.File)
	if err != nil {
		return nil, err
	}
	lfi := []logFileInfo{
		logFileInfo{
			Path: s.File,
			End:  f.ModTime(),
		},
	}

	// Try to find rotated logs.
	for i := 0; i < 100; i++ {
		var filename string
		for _, ext := range []string{"", ".gz"} {
			filename = fmt.Sprintf("%s.%d%s", s.File, i+1, ext)
			f, err = os.Stat(filename)
			if err == nil {
				break
			}
		}
		if err != nil {
			break
		}
		lfi = append(lfi, logFileInfo{
			Path: filename,
			End:  f.ModTime(),
		})
	}
	return lfi, nil
}

const syslogTimestampFormat = "Jan _2 15:04:05"

// Cache time.Now() and update it once a second.
var currentTime = time.Now()

func updateCurrentTime() {
	tick := time.NewTicker(1 * time.Second)
	for {
		currentTime = <-tick.C
	}
}

// This function can be replaced for testing.
var getCurrentTime = func() time.Time {
	return currentTime
}

// Parser for syslog logs (with standard parameters).
type syslogLineParser struct{}

func parseProgram(s string) string {
	s = strings.TrimSuffix(s, ":")
	i := strings.Index(s, "[")
	if i > 0 {
		s = s[:i]
	}
	return s
}

func (p *syslogLineParser) Parse(line []byte) (*Log, error) {
	if len(line) < 16 {
		return nil, errors.New("line too short")
	}

	tsstr := line[:15]
	rest := line[16:len(line)]

	ts, err := time.ParseInLocation(syslogTimestampFormat, string(tsstr), time.Local)
	if err != nil {
		return nil, err
	}

	// The timestamp in the log is missing the year, fill it in.
	now := getCurrentTime()
	if ts.Year() == 0 {
		ts = ts.AddDate(now.Year(), 0, 0)
	}
	if ts.After(now) {
		ts = ts.AddDate(-1, 0, 0)
	}

	// Parts are: host, program, message.
	parts := bytes.SplitN(rest, []byte{' '}, 3)
	if len(parts) < 2 {
		return nil, errors.New("no host")
	}
	l := &Log{
		Stamp: ts,
		Host:  string(parts[0]),
	}
	if len(parts) > 2 {
		l.Program = parseProgram(string(parts[1]))
		l.Message = string(parts[2])
	} else {
		l.Message = string(parts[1])
	}
	return l, nil
}

func newSyslogSource(filename string, opts *SourceOptions) Source {
	return newFileBasedSource(
		filename,
		&logfileScanner{File: filename},
		&syslogLineParser{})
}

// Generic parser for single-process logs.
type singleProcessLineParser struct {
	host       string
	program    string
	timeFormat string
}

func (p *singleProcessLineParser) Parse(line []byte) (*Log, error) {
	n := len(p.timeFormat)
	if len(line) < n+2 {
		return nil, errors.New("line too short")
	}

	ts, err := time.ParseInLocation(p.timeFormat, string(line[:n]), time.Local)
	if err != nil {
		return nil, err
	}

	return &Log{
		Stamp:   ts,
		Host:    p.host,
		Program: p.program,
		Message: string(line[n+1 : len(line)]),
	}, nil
}

func newSingleProcessSource(path string, opts *SourceOptions) Source {
	return newFileBasedSource(
		path,
		&logfileScanner{File: path},
		&singleProcessLineParser{
			host:       opts.Get("host", localhost),
			program:    opts.Get("prog", ""),
			timeFormat: opts.Get("time_fmt", time.RFC3339),
		})
}

// guardFile checks if a file has changed (closed and re-opened).
type guardFile struct {
	filename string
	info     os.FileInfo
}

func newGuardFile(filename string) *guardFile {
	return &guardFile{filename: filename}
}

func (g *guardFile) Changed() bool {
	f, err := os.Stat(g.filename)
	if err != nil {
		return true
	}

	result := true
	if g.info != nil {
		result = !os.SameFile(g.info, f)
	}
	g.info = f
	return result
}

type sourceFn func(string, *SourceOptions) Source

var formats = map[string]sourceFn{}

// NewSource creates a log Source of the given type pointing at the
// specified file.
func NewSource(sourceType, path string, opts *SourceOptions) (Source, error) {
	fn, ok := formats[sourceType]
	if !ok {
		return nil, errors.New("unknown source type")
	}
	return fn(path, opts), nil
}

func init() {
	// Setup the known formats table.
	formats["runit"] = newRunitSource
	formats["syslog"] = newSyslogSource
	formats["single"] = newSingleProcessSource

	// Set the local host name.
	localhost, _ = os.Hostname()

	// Start the epoch updater.
	go updateCurrentTime()
}

// MultiSource ties many Sources together.
type MultiSource struct {
	sources []Source
}

func (m *MultiSource) AddSource(src Source) {
	m.sources = append(m.sources, src)
}

type concatenateIter struct {
	iters []QueryResults
	cur   int
}

func (c *concatenateIter) Next() bool {
	for c.cur < len(c.iters) {
		i := c.iters[c.cur]
		ret := i.Next()
		if ret {
			return true
		}
		c.cur++
	}
	return false
}

func (c *concatenateIter) Log() *Log {
	return c.iters[c.cur].Log()
}

func (m *MultiSource) RunQuery(query Query) (QueryResults, error) {
	var partials []QueryResults
	for _, s := range m.sources {
		if r, err := s.RunQuery(query); err == nil {
			partials = append(partials, r)
		} else {
			log.Printf("RunQuery(%s): source error: %v", query.String(), err)
		}
	}
	if partials == nil {
		return nil, errors.New("all sources failed")
	}
	return &concatenateIter{iters: partials}, nil
}
