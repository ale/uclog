package uclog

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/rpc"
	"net/url"
	"sync"
	"time"

	"git.autistici.org/ale/gossip"
	"github.com/PuerkitoBio/ghost/handlers"
)

type gossipData struct{}

type shardedQuery struct {
	endpoints []string
}

func urlLineReader(url string, out chan string) error {
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	if resp.StatusCode != 200 {
		return errors.New("bad http status")
	}
	defer resp.Body.Close()
	r := bufio.NewScanner(resp.Body)
	for r.Scan() {
		out <- r.Text()
	}
	return nil
}

func (s *shardedQuery) query(q Query) chan string {
	ch := make(chan string, 100)
	var wg sync.WaitGroup

	for _, addr := range s.endpoints {
		wg.Add(1)
		go func(addr string) {
			uri := queryToRequest(addr, "/_query", q)
			if err := urlLineReader(uri, ch); err != nil {
				log.Printf("query: %s: %v", addr, err)
			}
			wg.Done()
		}(addr)
	}

	go func() {
		wg.Wait()
		close(ch)
	}()

	return ch
}

func (s *shardedQuery) update(members []gossip.Member) {
	var endpoints []string
	for _, m := range members {
		endpoints = append(endpoints, m.Addr)
	}
	s.endpoints = endpoints
	log.Printf("nodes: %v", endpoints)
}

type Server struct {
	ml *MultiSource
	sq *shardedQuery
}

func queryFromRequest(r *http.Request) (Query, error) {
	qstr := r.URL.Query().Get("q")
	if qstr == "" {
		return nil, errors.New("no search query")
	}

	var tr TimeRange
	var err error
	startStr := r.URL.Query().Get("start")
	if startStr != "" {
		tr.Start, err = ParseTimespec(startStr)
		if err != nil {
			return nil, err
		}
	}
	endStr := r.URL.Query().Get("end")
	if endStr != "" {
		tr.End, err = ParseTimespec(endStr)
		if err != nil {
			return nil, err
		}
	}

	return NewFieldQuery(qstr, tr)
}

func queryToRequest(addr, path string, q Query) string {
	v := make(url.Values)
	v.Set("q", q.String())
	tr := q.TimeRange()
	if !tr.Start.IsZero() {
		v.Set("start", tr.Start.Format(time.RFC3339))
	}
	if !tr.End.IsZero() {
		v.Set("end", tr.End.Format(time.RFC3339))
	}
	return fmt.Sprintf("http://%s%s?%s", addr, path, v.Encode())
}

func (s *Server) handleLocalQuery(w http.ResponseWriter, r *http.Request) {
	q, err := queryFromRequest(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	iter, err := s.ml.RunQuery(q)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "text/plain")
	for iter.Next() {
		if _, err := io.WriteString(w, iter.Log().String()); err != nil {
			break
		}
	}
}

func (s *Server) handleGlobalQuery(w http.ResponseWriter, r *http.Request) {
	q, err := queryFromRequest(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.Header().Set("Content-Type", "text/plain")
	ch := s.sq.query(q)
	for line := range ch {
		if _, err := fmt.Fprintln(w, line); err != nil {
			break
		}
	}
}

func (s *Server) runGossip(addr string, peers []string) {
	g := gossip.New(addr, &gossipData{}, peers)
	go g.Run()
	for {
		<-g.C
		s.sq.update(g.GetMembers())
	}
}

func (s *Server) Run(listenAddr, publicAddr string, peers []string) error {
	log.Printf("starting HTTP server on %s (public_addr=%s, peers=%v)", listenAddr, publicAddr, peers)

	s.sq.endpoints = []string{publicAddr}

	rpc.HandleHTTP()
	go s.runGossip(publicAddr, peers)

	mux := http.NewServeMux()
	mux.HandleFunc("/query", s.handleGlobalQuery)
	mux.HandleFunc("/_query", s.handleLocalQuery)
	http.Handle("/", handlers.GZIPHandler(mux, nil))

	return http.ListenAndServe(listenAddr, nil)
}

func NewServer(ml *MultiSource) *Server {
	return &Server{
		ml: ml,
		sq: &shardedQuery{},
	}
}
