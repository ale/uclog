package uclog

import (
	"log"
	"testing"
	"time"
)

func queryResultsToList(i QueryResults) []*Log {
	var out []*Log
	for i.Next() {
		out = append(out, i.Log())
	}
	return out
}

func runTestQuery(src Source, qstr string, tr TimeRange) []*Log {
	q, err := NewFieldQuery(qstr, tr)
	if err != nil {
		log.Fatal("NewFieldQuery", err)
	}
	i, err := src.RunQuery(q)
	if err != nil {
		log.Fatal("RunQuery", err)
	}
	return queryResultsToList(i)
}

func TestSource_Runit(t *testing.T) {
	src, err := ParseSource("runit:testdata/runit prog=testprog")
	if err != nil {
		t.Fatal(err)
	}

	results := runTestQuery(src, "4473881", TimeRange{
		Start: time.Date(2014, 10, 21, 0, 0, 0, 0, time.UTC),
	})
	if len(results) != 3 {
		t.Fatalf("Expected 3 results, got: %+v", results)
	}
	if results[0].Program != "testprog" {
		t.Errorf("Program not set in Log: %v (expected testprog)", results[0].Program)
	}

	results = runTestQuery(src, "4473881", TimeRange{
		Start: time.Date(2014, 10, 20, 0, 0, 0, 0, time.UTC),
		End:   time.Date(2014, 10, 21, 0, 0, 0, 0, time.UTC),
	})
	if len(results) != 0 {
		t.Fatalf("Expected no results, got: %+v", results)
	}
}

func TestSource_SingleProc(t *testing.T) {
	src, err := ParseSource("single:testdata/singleproc/icecast.log prog=testprog time_fmt=\"[2006-01-02  15:04:05]\"")
	if err != nil {
		t.Fatal(err)
	}

	results := runTestQuery(src, "ondarossa.mp3 exiting", TimeRange{
		Start: time.Date(2014, 10, 19, 0, 0, 0, 0, time.UTC),
	})
	if len(results) != 1 {
		t.Fatalf("Expecting 1 result, got: %+v", results)
	}
}

func TestSource_Syslog(t *testing.T) {
	src, err := ParseSource("syslog:testdata/syslog/messages")
	if err != nil {
		t.Fatal(err)
	}

	now := time.Now()
	curTime := time.Date(now.Year(), time.October, 24, 8, 0, 0, 0, time.Local)
	getCurrentTime = func() time.Time { return curTime }

	results := runTestQuery(src, "debian-sa1", TimeRange{
		Start: time.Date(2014, 10, 19, 0, 0, 0, 0, time.UTC),
	})
	if len(results) != 2 {
		t.Fatalf("Expecting 2 results, got: %+v", results)
	}
}
