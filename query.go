package uclog

import (
	"fmt"
	"strings"
)

type Query interface {
	Match(*Log) bool
	String() string
	TimeRange() TimeRange
}

type fieldQuery struct {
	qstr         string
	tr           TimeRange
	programMatch []string
	hostMatch    []string
	messageMatch []string
}

func matchAll(keywords []string, s string) bool {
	for _, k := range keywords {
		if !strings.Contains(s, k) {
			return false
		}
	}
	return true
}

func (q *fieldQuery) TimeRange() TimeRange {
	return q.tr
}

func (q *fieldQuery) String() string {
	return q.qstr
}

func (q *fieldQuery) Match(l *Log) bool {
	if !q.tr.Contains(l.Stamp) {
		return false
	}
	if q.programMatch != nil && !matchAll(q.programMatch, l.Program) {
		return false
	}
	if q.hostMatch != nil && !matchAll(q.hostMatch, l.Host) {
		return false
	}
	if q.messageMatch != nil && !matchAll(q.messageMatch, l.Message) {
		return false
	}
	return true
}

// Parse a field query (consisting of field:value tokens). The default
// field, if unspecified, is 'message'. All elements of the query are
// logically ANDed together.
func NewFieldQuery(s string, t TimeRange) (Query, error) {
	q := fieldQuery{qstr: s, tr: t}
	for _, token := range SplitQuoted(s) {
		field := "message"
		if strings.Contains(token, ":") {
			tparts := strings.SplitN(token, ":", 2)
			field = tparts[0]
			token = tparts[1]
		}
		if token == "" {
			continue
		}
		switch field {
		case "message":
			q.messageMatch = append(q.messageMatch, token)
		case "prog", "program":
			q.programMatch = append(q.programMatch, token)
		case "host":
			q.hostMatch = append(q.hostMatch, token)
		default:
			return nil, fmt.Errorf("unknown field '%s'", field)
		}
	}
	return &q, nil
}
