package uclog

import (
	"bufio"
	"errors"
	"log"
	"os"
	"path/filepath"
	"strings"
)

func isGlob(s string) bool {
	return strings.Contains(s, "*")
}

var configSeen = map[string]struct{}{}

// ReadConfig parses a configuration file containing source
// specifications. The configuration file can include other files,
// possibly using filesystem wildcards, and it can contain comments
// (lines starting with a #).
func (m *MultiSource) ReadConfig(path string) error {
	if _, ok := configSeen[path]; ok {
		return errors.New("inclusion loop")
	}
	configSeen[path] = struct{}{}

	f, err := os.Open(path)
	if err != nil {
		return err
	}
	defer f.Close()

	rows := bufio.NewScanner(f)
	for rows.Scan() {
		line := strings.TrimSpace(rows.Text())
		if line == "" || strings.HasPrefix(line, "#") {
			continue
		}
		if strings.HasPrefix(line, "include ") {
			inc := strings.Fields(line)[1]
			files, err := filepath.Glob(inc)
			if err != nil {
				return err
			}
			for _, f := range files {
				m.ReadConfig(f)
			}
		}
		src, err := ParseSource(line)
		if err != nil {
			log.Printf("Warning: %s: could not parse source '%s', skipped", path, line)
		}
		m.AddSource(src)
	}
	return nil
}
