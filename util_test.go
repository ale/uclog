package uclog

import (
	"testing"
)

func TestParseSource_Parse(t *testing.T) {
	okSrcs := []string{
		"/var/log/syslog",
		"syslog:/var/log/syslog",
		"runit:/var/log/test-service",
		"runit:/var/log/test-service prog=test",
		"runit:/var/log/test-service prog=\"test prog\"",
	}
	for _, src := range okSrcs {
		_, err := ParseSource(src)
		if err != nil {
			t.Errorf("Error parsing \"%s\": %v", src, err)
		}
	}
}

func TestParseSourceOptions_Parse(t *testing.T) {
	optStr := "a=42 b=\"value with spaces\" c"
	opts, err := parseSourceOptions(optStr)
	if err != nil {
		t.Fatal(err)
	}
	a := opts.Get("a", "")
	if a != "42" {
		t.Errorf("'a' = %s (expected: 42)", a)
	}
	b := opts.Get("b", "")
	if b != "value with spaces" {
		t.Errorf("'b' = %s (expected: value with spaces)", b)
	}
	c := opts.Get("c", "")
	if c != "true" {
		t.Errorf("'c' = %s (expected: true)", c)
	}
}
