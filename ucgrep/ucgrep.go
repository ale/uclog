package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"strings"
	"time"
)

var (
	server   = flag.String("server", "localhost:1033", "server to connect to")
	fromTime = flag.String("from", time.Now().Add(-2*time.Hour).Format(time.RFC3339), "Start time")
	toTime   = flag.String("to", "", "End time")
)

func main() {
	flag.Parse()

	if flag.NArg() == 0 {
		log.Fatal("You should specify a query.")
	}

	query := strings.Join(flag.Args(), " ")

	v := make(url.Values)
	v.Set("q", query)
	v.Set("start", *fromTime)
	if *toTime != "" {
		v.Set("end", *toTime)
	}
	uri := fmt.Sprintf("http://%s/query?%s", *server, v.Encode())

	resp, err := http.Get(uri)
	if err != nil {
		log.Fatal(err)
	}
	if resp.StatusCode != 200 {
		log.Fatalf("HTTP error: %s", resp.Status)
	}
	defer resp.Body.Close()

	sortCmd := exec.Command("sort", "--key=1")
	sortCmd.Stdout = os.Stdout
	stdin, err := sortCmd.StdinPipe()
	if err != nil {
		log.Fatal(err)
	}

	if err := sortCmd.Start(); err != nil {
		log.Fatal(err)
	}

	io.Copy(stdin, resp.Body)
	stdin.Close()

	if err := sortCmd.Wait(); err != nil {
		log.Fatal(err)
	}
}
