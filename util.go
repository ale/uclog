package uclog

import (
	"errors"
	"strings"
	"time"
	"unicode"
)

// SplitQuoted splits a string on whitespace, but it won't break
// quoted strings. Note: this simple quoted string splitter won't let
// you quote quotes themselves, but it lets you put quotes in the
// middle of the string.
func SplitQuoted(s string) []string {
	var lastQuote rune
	f := func(c rune) bool {
		switch {
		case c == lastQuote:
			lastQuote = rune(0)
			return false
		case lastQuote != rune(0):
			return false
		case c == '"':
			lastQuote = c
			return false
		default:
			return unicode.IsSpace(c)
		}
	}
	var out []string
	for _, field := range strings.FieldsFunc(s, f) {
		out = append(out, strings.Replace(field, "\"", "", -1))
	}
	return out
}

func parseSourceOptions(s string) (*SourceOptions, error) {
	opts := &SourceOptions{
		values: make(map[string]string),
	}
	parts := SplitQuoted(s)
	for _, p := range parts {
		if !strings.Contains(p, "=") {
			p = p + "=true"
		}
		pp := strings.SplitN(p, "=", 2)
		opts.values[pp[0]] = pp[1]
	}
	return opts, nil
}

// ParseSource returns a Source based on the string specification
// provided, which should be in the format "type:path". A source
// specification might include a space-separated list of options
// in 'key=value' format (the special syntax 'key' implies
// 'key=true').
func ParseSource(spec string) (Source, error) {
	var opts *SourceOptions
	parts := strings.SplitN(spec, " ", 2)
	if len(parts) > 1 {
		var err error
		opts, err = parseSourceOptions(parts[1])
		if err != nil {
			return nil, err
		}
	}
	spec = parts[0]
	if !strings.Contains(spec, ":") {
		return NewSource("syslog", spec, opts)
	}
	parts = strings.SplitN(spec, ":", 2)
	if len(parts) != 2 {
		return nil, errors.New("bad source spec")
	}
	return NewSource(parts[0], parts[1], opts)
}

var timeFormats = []string{
	time.RFC3339,
	"2006-01-02T15:04:05",
	"2006/01/02 15:04",
	"2006-01-02 15:04",
	"2006/01/02",
	"2006-01-02",
}

// ParseTimespec parses time in one of many possible formats.
func ParseTimespec(s string) (time.Time, error) {
	for _, fmt := range timeFormats {
		t, err := time.Parse(fmt, s)
		if err == nil {
			return t, nil
		}
	}
	return time.Time{}, errors.New("could not parse time spec")
}
