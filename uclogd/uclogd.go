package main

import (
	"flag"
	"log"
	"net"
	"os"
	"strings"

	"git.autistici.org/ale/uclog"
)

var (
	configFile = flag.String("config", "", "system logs config")
	addr       = flag.String("addr", ":1033", "address to listen on")
	publicAddr = flag.String("public-addr", "", "address to advertise (default: autodetected)")
	peers      = flag.String("peers", "", "peer addresses (comma separated)")
)

func findLocalIP() string {
	hostname, err := os.Hostname()
	if err != nil {
		log.Fatal(err)
	}
	addrs, err := net.LookupIP(hostname)
	if err != nil {
		log.Fatal(err)
	}
	for _, ip := range addrs {
		if ip.IsLoopback() || ip.IsLinkLocalUnicast() {
			continue
		}
		return ip.String()
	}
	log.Fatal("could not find the public IP address of the local host")
	return ""
}

func findPublicAddr() string {
	host, port, err := net.SplitHostPort(*addr)
	if err != nil {
		log.Fatal(err)
	}
	if host == "" {
		host = findLocalIP()
	}
	return net.JoinHostPort(host, port)
}

func main() {
	flag.Parse()

	peerList := strings.Split(*peers, ",")
	public := *publicAddr
	if public == "" {
		public = findPublicAddr()
	}

	if *configFile == "" {
		log.Fatal("Must specify --config")
	}

	ml := &uclog.MultiSource{}
	if err := ml.ReadConfig(*configFile); err != nil {
		log.Fatal(err)
	}

	srv := uclog.NewServer(ml)
	log.Fatal(srv.Run(*addr, public, peerList))
}
