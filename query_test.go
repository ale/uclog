package uclog

import (
	"reflect"
	"testing"
	"time"
)

func TestFieldQuery_Parse(t *testing.T) {
	testdata := []struct {
		qstr                 string
		expectedMessageMatch []string
		expectedHostMatch    []string
		expectedProgramMatch []string
	}{
		{"cron", []string{"cron"}, nil, nil},
		{"\"cron\"", []string{"cron"}, nil, nil},
		{"two words", []string{"two", "words"}, nil, nil},
		{"\"one word\"", []string{"one word"}, nil, nil},
		{"message:test", []string{"test"}, nil, nil},
		{"host:test", nil, []string{"test"}, nil},
		{"prog:test", nil, nil, []string{"test"}},
		{"prog:test error msg", []string{"error", "msg"}, nil, []string{"test"}},
	}

	for _, td := range testdata {
		q, err := NewFieldQuery(td.qstr, TimeRange{})
		if err != nil {
			t.Fatalf("parsing '%s': %v", td.qstr, err)
		}
		fq := q.(*fieldQuery)
		if !reflect.DeepEqual(fq.messageMatch, td.expectedMessageMatch) {
			t.Errorf("Parse error (message match): '%s' -> %v (expected: %v)", td.qstr, fq.messageMatch, td.expectedMessageMatch)
		}
		if !reflect.DeepEqual(fq.hostMatch, td.expectedHostMatch) {
			t.Errorf("Parse error (host match): '%s' -> %v (expected: %v)", td.qstr, fq.hostMatch, td.expectedHostMatch)
		}
		if !reflect.DeepEqual(fq.programMatch, td.expectedProgramMatch) {
			t.Errorf("Parse error (program match): '%s' -> %v (expected: %v)", td.qstr, fq.programMatch, td.expectedProgramMatch)
		}

	}
}

func TestFieldQuery_Match(t *testing.T) {
	q, _ := NewFieldQuery("prog:test error msg", TimeRange{Start: time.Date(2014, 10, 22, 0, 0, 0, 0, time.UTC), End: time.Date(2014, 10, 22, 23, 59, 59, 0, time.UTC)})

	testdata := []struct {
		log         *Log
		expectMatch bool
	}{
		{&Log{Program: "foo", Message: "bar", Stamp: time.Date(1980, 1, 1, 0, 0, 0, 0, time.UTC)}, false},
		{&Log{Program: "test", Message: "an error msg", Stamp: time.Date(1980, 1, 1, 0, 0, 0, 0, time.UTC)}, false},
		{&Log{Program: "test", Message: "some other msg", Stamp: time.Date(2014, 10, 22, 13, 0, 0, 0, time.UTC)}, false},
		{&Log{Program: "test", Message: "an error msg", Stamp: time.Date(2014, 10, 22, 13, 0, 0, 0, time.UTC)}, true},
	}

	for _, td := range testdata {
		result := q.Match(td.log)
		if result != td.expectMatch {
			t.Errorf("Match(%s, %+v) -> %v (expected %v)", q.String(), td.log, result, td.expectMatch)
		}
	}
}
