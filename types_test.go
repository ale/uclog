package uclog

import (
	"testing"
	"time"
)

func TestTimeRange_Contains(t *testing.T) {
	now := time.Now()

	r := TimeRange{now.AddDate(0, -1, 0), now.AddDate(0, 0, -1)}
	ts := now.AddDate(0, 0, -7)
	if !r.Contains(ts) {
		t.Errorf("range %v does not contain %v", r, ts)
	}

	r = TimeRange{Start: now.AddDate(0, -1, 0)}
	if !r.Contains(ts) {
		t.Errorf("range %v does not contain %v", r, ts)
	}
}

func TestTimeRange_Overlaps(t *testing.T) {
	now := time.Now()

	r := TimeRange{now.AddDate(0, -1, 0), now.AddDate(0, 0, -1)}
	r2 := TimeRange{now.AddDate(0, 0, -7), now.AddDate(0, 0, -6)}
	if !r.Overlaps(r2) {
		t.Errorf("range %v does not overlap %v", r, r2)
	}
	if r.Overlaps(r2) != r2.Overlaps(r) {
		t.Error("Overlaps is not commutative")
	}

	r2 = TimeRange{Start: now.AddDate(0, -7, 0)}
	if !r.Overlaps(r2) {
		t.Errorf("range %v does not overlap %v", r, r2)
	}
	if r.Overlaps(r2) != r2.Overlaps(r) {
		t.Error("Overlaps is not commutative with partial ranges")
	}
}
