// Uclog is a small package to distribute log searching among multiple
// hosts and multiple log sources.
//
package uclog

import (
	"fmt"
	"time"
)

// A log entry. Just a timestamped message with some standard metadata.
type Log struct {
	Host    string
	Program string
	Message string
	Stamp   time.Time
}

// String returns a string representation of the log (including a
// terminating newline).
func (l Log) String() string {
	return fmt.Sprintf("%s %s %s %s\n", l.Stamp.UTC().Format(time.RFC3339), l.Host, l.Program, l.Message)
}

// Time range. Start and End can be zero, which represents an
// open-ended range.
type TimeRange struct {
	Start time.Time
	End   time.Time
}

// Overlaps returns true if t and t2 overlap.
func (t TimeRange) Overlaps(t2 TimeRange) bool {
	return (t.Start.IsZero() || t2.End.IsZero() || t.Start.Before(t2.End)) && (t.End.IsZero() || t2.Start.IsZero() || t.End.After(t2.Start))
}

// Contains returns true if tt is contained within the time range.
func (t TimeRange) Contains(tt time.Time) bool {
	return (t.Start.IsZero() || t.Start.Before(tt)) && (t.End.IsZero() || t.End.After(tt))
}

// Query result iterator.
type QueryResults interface {
	Next() bool
	Log() *Log
}

// A local log source. In practical terms this is an interface to
// anything that can evaluate a Query and return logs as a result.
type Source interface {
	RunQuery(Query) (QueryResults, error)
}
